<main class="cv">

  <!-- 70vh sous le menu, background-image fixed -->

  <section class="header">

    <h3>Webdesign <br>
      développement <br>
      graphisme <br>
    </h3>

  </section>

  <!-- Trois déroulants avec la description des services et les compétences techniques -->

  <h3 class="compettitle">Création, refonte de site, identité visuelle</h3>

  <section id="competences" class="competences">

    <article>

      <span class="splog"><img src="gallery/trucsquichangentpas/logocv.png" alt=""></span>
      <h3>Webdesign, ux/ui design</h3>
      <p class="competext show">
        Le WebDesign est essentiel dans la conception d’un site Internet. Il est l’identité visuelle d'un site web et doit prendre en compte les attentes et les besoins de l’internaute, être facile à trouver, accessible, donner envie, confiance et facile à prendre en main.
      </p>
      <span class="spcar"><img class="caret" src="gallery/trucsquichangentpas/caret.png" alt=""></span>
      <p class="competext hide">Axure / HTML5 / CSS3 / Jquery / Javascript<br>
        <br>
        Réalisation de maquettes Web.<br>
        Réalisation de sites.<br>
        Ergonomie, architecture de l’information.<br>
        Développement au service de l’UX Design.
      </p>

    </article>

    <article>

      <span class="splog"><img src="gallery/trucsquichangentpas/logocv1.png" alt=""></span>
      <h3>Développement</h3>
      <p class="competext show">
        De la page statique à la page dynamique avec connexion à une base de données, je développe des sites et quelle que soit la technologie choisie, votre site Internet s’adaptera à tous les écrans.
      </p>
      <span class="spcar"><img class="caret" src="gallery/trucsquichangentpas/caret.png" alt=""></span>
      <p class="competext hide"> PHP5 / SQL / MySQL <br>
        <br>
        CMS (wordpress).<br>
        Programmation PHP.<br>
        Conception de bases de données (MYSQL)CMS (wordpress).<br>
        Programmation PHP.<br>
        Conception de bases de données (MYSQL).
      </p>

    </article>

    <article>

      <span class="splog"><img src="gallery/trucsquichangentpas/logocv2.png" alt=""></span>
      <h3>Graphisme et motion design</h3>
      <p class="competext show">
        Conception et réalisation d'identité visuelle, logotype : déclinaison sur tous supports papiers. Création de plaquettes, affiches, flyers. Conception et mise en page de brochures, journaux. Création d'infographies.
      </p>
      <span class="spcar"><img class="caret" src="gallery/trucsquichangentpas/caret.png" alt=""></span>
      <p class="competext hide">Conception et réalisation <br>
        de productions animées graphique en 2D.<br>
        Infographies.<br>
        Elaboration d’une charte visuelle.<br>
        Mise en page et conception graphique.<br>
        Retouche photos.<br>
        Recherche iconographique.<br>
        Connaissance des règles typographiques. <br>
      </p>

    </article>

  </section>

  <!-- 70vh avec background-image fixed, liste les compétences techniques -->

  <section class="tags">
    <h2>
      Axure / HTML5 / CSS3 / Jquery / Javascript <br>
      PHP5 / MySQL / After effect / Audition <br>
      Première Pro / Indesign / Photoshop / Illustrator<br>
      Acrobat Pro / PitStop pro / Callas
    </h2>
  </section>

  <!-- Présentation et parcours de Sophie -->

  <section id="aboutme" class="aboutme">
    <p>Après avoir travaillé dans le secteur de l'impression offset, j'ai choisi de me reconvertir dans le digital.
      Passionnée par les nouvelles technologies multimédia, j'ai suivi une formation de chef de projet multimédia
      à l'IESA multimédia. Qu’il s’agisse d’un projet : webdesign, ux/ui design, intégration web, développement web
      ou graphisme print, n’hésitez pas à me contacter. Quel que soit votre secteur d’activité, j’apporterai toute
      ma rigueur et mon professionnalisme afin de répondre à vos attentes, dans le respect des délais impartis.
    </p>
    <h6>About <br> me</h6>
  </section>

  <!-- Formulaire de contact, ne fonctionne pas -->

  <section id="contact" class="contact">
    <p>Me <br> contacter</p>
    <form class="" action="contact.php" method="post">
      <input type="text" name="nom" placeholder="Nom, prénom"/><br />
      <input type="text" name="email" placeholder="Adresse email"/><br />
      <textarea name="message" rows="5" cols="40" placeholder="Message"></textarea><br>
      <button type="submit" name="envoi">Envoyer</button>
    </form>

  </section>
