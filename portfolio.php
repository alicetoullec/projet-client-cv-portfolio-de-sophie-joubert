<main class="ptf">

  <!-- Trois onglets contenant chacun trois images-exemples du travail de Sophie -->

  <!-- Code JS dans script.js -->

  <div class="systeme_onglets">

    <div class="onglets">
      <span class="onglet_0 onglet" id="onglet_web" onclick="javascript:change_onglet('web');">Web</span>
      <span class="onglet_0 onglet" id="onglet_graphisme" onclick="javascript:change_onglet('graphisme');">Graphisme</span>
      <span class="onglet_0 onglet" id="onglet_print" onclick="javascript:change_onglet('print');">Print</span>
    </div>

    <div class="contenu_onglets">

      <section class="contenu_onglet" id="contenu_onglet_web">
        <div class="header">
          <h1>Création <br> et refonte <br> de sites</h1>
        </div>
        <article>
          <img src="gallery/web1.png" alt="">
          <img src="gallery/web2.png" alt="">
          <img src="gallery/web3.png" alt="">
        </article>
      </section>

      <section class="contenu_onglet" id="contenu_onglet_graphisme">
        <div class="header">
          <h1>Infographies <br> Créations de logo </h1>
        </div>
        <article>
          <img src="gallery/graphisme1.png" alt="">
          <img src="gallery/graphisme2.png" alt="">
          <img src="gallery/graphisme3.png" alt="">
        </article>
      </section>

      <section class="contenu_onglet" id="contenu_onglet_print">
        <div class="header">
          <h1>Création de Maquettes : Kakémono, affiches, <br> brochures/4 pages/executive summary <br> flyers/cartes de correspondances</h1>
        </div>
        <article>
          <img src="gallery/print1.png" alt="">
          <img src="gallery/print2.png" alt="">
          <img src="gallery/print3.png" alt="">
        </article>
      </section>

    </div>
  </div>

  <!-- Si on le met dans le script.js, par défaut, aucun onglet ne sera ouvert -->

  <script type="text/javascript">
  var anc_onglet = 'web';
  change_onglet(anc_onglet);
  </script>

</main>
