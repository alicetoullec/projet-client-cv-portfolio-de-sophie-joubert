// Pour les onglets du portfolio

function change_onglet(name){
  document.getElementById('onglet_'+anc_onglet).className = 'onglet_0 onglet';
  document.getElementById('onglet_'+name).className = 'onglet_1 onglet';
  document.getElementById('contenu_onglet_'+anc_onglet).style.display = 'none';
  document.getElementById('contenu_onglet_'+name).style.display = 'block';
  anc_onglet = name;
}

var anc_onglet = 'web';
change_onglet(anc_onglet);

// Dans la section "competences" de la page CV, il faudrait, en JQuery, que les
// classes "hide" soient changées en "show" grâce au clic ou au toucher sur
// tablette et mobile.
