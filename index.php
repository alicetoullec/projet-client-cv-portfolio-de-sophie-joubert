<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">

  <!-- Titre en fonction de la page affichée -->

  <title>

    <?php
    switch($_GET["page"]){
      case "cv.php";
      echo "S.Joubert : CV";
      break;
      case "portfolio.php";
      echo "S.Joubert : Portfolio";
      break;
      default;
      echo "ERROR : PAGE NOT FOUND";
      break;
    }?>

  </title>
  <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Roboto:400,700,900" rel="stylesheet">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script src="script.js">

  </script>
  <link rel="stylesheet" href="style.css">
</head>
<body>

  <!-- Header avec logo, menu principal & menu des réseaux sociaux -->

  <header id="header">

    <nav>
      <a class="logo" href="http://localhost/projetsEnVrac/sophie/index.php?page=cv.php#header"><h1> Sophie Joubert <br> Web Designer </h1></a>
      <ul>
        <a href="http://localhost/projetsEnVrac/sophie/index.php?page=cv.php#competences"><li>Services & compétences</li></a>
        <a href="http://localhost/projetsEnVrac/sophie/index.php?page=cv.php#aboutme"><li>About me</li></a>
        <a href="http://localhost/projetsEnVrac/sophie/index.php?page=cv.php#contact"><li>Contact</li></a>
        <a href="http://localhost/projetsEnVrac/sophie/index.php?page=portfolio.php"><li>portfolio</li></a>
      </ul>
    </nav>

  </header>

  <!-- Contenu de la page (CV ou portfolio) -->

  <?php
  $listPages = array('cv.php', 'portfolio.php');

  if (isset ($_GET ['page']) && in_array($_GET ['page'], $listPages)) {
    include $_GET['page'];
  }
  else {
    echo "<p class=\"errormessage\">ERROR : PAGE NOT FOUND</p>";
  }
  ?>

  <!-- Footer -->

  <footer>
    <p class="mentions">Mentions légales</p> <p class="signature">footer</p>
  </footer>

</body>
</html>
